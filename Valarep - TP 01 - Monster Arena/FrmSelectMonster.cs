﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Valarep___TP_01___Monster_Arena
{
    public partial class FrmSelectMonster : Form
    {
        public Image[] Images { get; set; }
        public Monster SelectedMonster { get; private set; }

        public FrmSelectMonster()
        {
            InitializeComponent();
        }

        private void FrmChoixDuMonstre_Load(object sender, EventArgs e)
        {
            // todo 02 : Commenter chaque ligne de cette procédure
            int index = 0;
            foreach(PictureBox pictureBox in panel1.Controls)
            {
                pictureBox.Image = Images[index];
                pictureBox.Tag = index;
                index++;
            }
        }

        private void PictureBox_Click(object sender, EventArgs e)
        {
            // todo 03 : Commenter chaque ligne de cette procédure

            PictureBox pictureBox = (PictureBox)sender;

            int index = (int) pictureBox.Tag;
            Image image = Images[index];

            SelectedMonster = Monster.Create(index, image);

            DialogResult = DialogResult.OK;
        }
    }
}

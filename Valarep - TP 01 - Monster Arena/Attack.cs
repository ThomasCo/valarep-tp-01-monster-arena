﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valarep___TP_01___Monster_Arena
{
    public class Attack
    {
        public string Name { get; private set; }
        public int Damage { get; private set; }

        public static Attack HornAttack(int strength)
        {
            return new Attack() { Name = "Horn Attack", Damage = strength / 5};
        }

        public static Attack BiteAttack(int strength)
        {
            return new Attack() { Name = "Bite Attack", Damage = strength / 10 };
        }

        public static Attack ClawAttack(int strength)
        {
            Random rnd = new Random(Convert.ToInt32(DateTime.Now.ToBinary()));
            int d10 = rnd.Next(1, 10);
            int damage = (d10 == 10)? 20 : d10 + 2;
            return new Attack() { Name = "Claw Attack", Damage = damage };
        }

        public static Attack EyesAttack(int strength)
        {
            Random rnd = new Random(Convert.ToInt32(DateTime.Now.ToBinary()));
            int d6 = rnd.Next(1, 6);
            int damage = d6 + 4;
            return new Attack() { Name = "Eyes Attack", Damage = damage };
        }

        public static Attack HeadAttack(int strength)
        {
            Random rnd = new Random(Convert.ToInt32(DateTime.Now.ToBinary()));
            int d4 = rnd.Next(1, 4);
            int damage = d4 + strength / 2;
            return new Attack() { Name = "Head Attack", Damage = damage };
        }

        public static Attack MadnessAttack(int strength)
        {
            Random rnd = new Random(Convert.ToInt32(DateTime.Now.ToBinary()));
            int d10 = rnd.Next(1, 10);
            int damage = d10 + strength / 5;
            return new Attack() { Name = "Madness Attack", Damage = damage };
        }
    }
}
